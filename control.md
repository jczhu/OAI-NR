# Controlling Your Experiment

The UE and enb services can be controlled from the command
line by logging into the appropriate nodes. In addition, all nodes in
the experiment can be rebooted or reloaded (reload means loading a
fresh disk image wiping out any changes) from the portal
interface.


## eNodeB Control

You can start the enb service by hand by running:

    sudo /local/repository/bin/enb.start.sh

Or you can kill a running enb service with:

    sudo /local/repository/bin/enb.kill.sh



## UE Control

You should start the UE service after the enb 
service is running on 'enb1' node. You can start the enb
service by hand by running:

    sudo /local/repository/bin/ue.start.sh




## Tweaking Configuration

The source template for the configuration file is at
`/local/repository/etc/enb.conf`. It is used only once when the
experiment boots for the first time. It in turn generate the
configuration file which are located at `/usr/local/etc/oai/enb.conf`
on `enb1` node. The generated configuration file
can be modified, but those changes will be lost if the node is
reloaded, the experiment ends, or you force them to be regenerated
from the template.

To change configuration permanently, you will want to fork the
repository for this profile, make a copy of it, and change the
configuration files in your forked repo.
